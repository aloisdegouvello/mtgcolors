init:
	pip3 install -r requirements.txt

lint:
	@black . 

test:
	python3 -m unittest discover -p '*test.py'

build: clean
	python3 setup.py sdist bdist_wheel
	@echo 'creating .pypirc'
	@sed -e 's/{username}/$(u)/g' -e 's/{password}/$(p)/g' .pypirc > ~/.pypirc

clean:
	rm -rf ~/.pypirc
	rm -rf build
	rm -rf dist
	rm -rf *.egg-info
	rm -rf ./mtgcolors/__pycache__
	rm -rf ./tests/__pycache__
	rm -f mtgcolors/*.pyc
	rm -f tests/*.pyc

deploy:
	python3 -m twine upload --repository $(repository) --verbose dist/*

.PHONY: init test