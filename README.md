# mtgcolors

## How to use

    // to do

## Why?

In the Discord Server we use, we have players with various level of mtg's knowledge.
Colors are a core part of Magic The Gathering. From the start of the game, different colors' surnames raised and fell. It can be difficult to keep track of all of them. Some illustrations of this struggle can be found on StackExchange where the most popular question is "[What are the names for Magic's different colour combinations?](https://boardgames.stackexchange.com/questions/11550/what-are-the-names-for-magics-different-colour-combinations)" or on Reddit where we often see post about colors like this [heads up](https://www.reddit.com/r/MagicArena/comments/clo25c/just_a_heads_up/).

Beside, this is also a way to practice my Python and to learn how to write a Discord Bot. Any help is welcome!

## License

[GPLv3](https://gitlab.com/aloisdegouvello/mtgcolors/raw/master/LICENSE.txt)