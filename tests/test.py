# -*- coding: utf-8 -*-

from .context import mtgcolors
import unittest


class TestMtgColorsMethods(unittest.TestCase):
    def test_no_argument_found(self):
        self.assertRaises(ValueError, mtgcolors.clarify, None)
        self.assertRaises(ValueError, mtgcolors.clarify, "")
        self.assertRaises(ValueError, mtgcolors.clarify, " ")
        self.assertRaises(ValueError, mtgcolors.clarify, None, True)
        self.assertRaises(ValueError, mtgcolors.clarify, "", True)
        self.assertRaises(ValueError, mtgcolors.clarify, " ", True)

    def test_not_found(self):
        self.assertEqual(mtgcolors.clarify("key_not_found"), "key not found")
        self.assertEqual(mtgcolors.clarify("123"), "key not found")

    def test_1_color_cases(self):
        self.assertEqual(mtgcolors.clarify("White"), "White")
        self.assertEqual(mtgcolors.clarify("white"), "White")
        self.assertEqual(mtgcolors.clarify("wHiTe"), "White")
        self.assertEqual(mtgcolors.clarify("Blue"), "Blue")
        self.assertEqual(mtgcolors.clarify("Black"), "Black")
        self.assertEqual(mtgcolors.clarify("Red"), "Red")
        self.assertEqual(mtgcolors.clarify("Green"), "Green")

    def test_2_colors_cases(self):
        pairs = {
            "Azorius": "Azorius (WU, White + Blue)",
            "azorius": "Azorius (WU, White + Blue)",
            "aZoRiUs": "Azorius (WU, White + Blue)",
            "AZORIUS": "Azorius (WU, White + Blue)",
            "Boros": "Boros (WR, White + Red)",
            "Dimir": "Dimir (UB, Blue + Black)",
            "Golgari": "Golgari (BG, Black + Green)",
            "Gruul": "Gruul (RG, Red + Green)",
            "Izzet": "Izzet (UR, Blue + Red)",
            "Orzhov": "Orzhov (WB, White + Black)",
            "Rakdos": "Rakdos (BR, Black + Red)",
            "Selesnya": "Selesnya (WG, White + Green)",
            "Simic": "Simic (UG, Green + Blue)",
        }
        for argument, expected in pairs.items():
            with self.subTest(i=argument):
                self.assertEqual(mtgcolors.clarify(argument), expected)

    def test_3_colors_cases(self):
        pairs = {
            "Abzan": "Abzan (WBG, White + Black + Green)",
            "abzan": "Abzan (WBG, White + Black + Green)",
            "aBzAn": "Abzan (WBG, White + Black + Green)",
            "ABZAN": "Abzan (WBG, White + Black + Green)",
            "Jeskai": "Jeskai (WUR, White + Blue + Red)",
            "Sultai": "Sultai (UBG, Blue + Black + Green)",
            "Mardu": "Mardu (WBR, White + Black + Red)",
            "Temur": "Temur (URG, Blue + Red + Green)",
            "Bant": "Bant (WUG, White + Blue + Green)",
            "Esper": "Esper (WUB, White + Blue + Black)",
            "Grixis": "Grixis (UBR, Blue + Black + Red)",
            "Jund": "Jund (BRG, Black + Red + Green)",
            "Naya": "Naya (WRG, White + Red + Green)",
        }
        for argument, expected in pairs.items():
            with self.subTest(i=argument):
                self.assertEqual(mtgcolors.clarify(argument), expected)

    # https://magic.wizards.com/en/articles/archive/card-preview/designing-commander-2016-edition-2016-10-24
    def test_4_colors_cases(self):
        pairs = {
            "Artifice": "Artifice (WUBR, White + Blue + Black + Red)",
            "artifice": "Artifice (WUBR, White + Blue + Black + Red)",
            "aRtIfIcE": "Artifice (WUBR, White + Blue + Black + Red)",
            "ARTIFICE": "Artifice (WUBR, White + Blue + Black + Red)",
            "Chaos": "Chaos (UBRG, Blue + Black + Red + Green)",
            "Aggression": "Aggression (BRGW, Black + Red + Green + White)",
            "Altruism": "Altruism (RGWU, Red + Green + White + Blue)",
            "Growth": "Growth (GWUB, Green + White + Blue + Black)",
        }
        for argument, expected in pairs.items():
            with self.subTest(i=argument):
                self.assertEqual(mtgcolors.clarify(argument), expected)

    def test_2_letters_colors_cases(self):
        expected = "Azorius (WU, White + Blue)"
        self.assertEqual(mtgcolors.clarify("wu"), expected)
        self.assertEqual(mtgcolors.clarify("uw"), expected)
        self.assertEqual(mtgcolors.clarify("Wu"), expected)
        self.assertEqual(mtgcolors.clarify("uW"), expected)
        self.assertEqual(mtgcolors.clarify("UW"), expected)
        self.assertEqual(mtgcolors.clarify("WU"), expected)
        self.assertEqual(mtgcolors.clarify("rw"), "Boros (WR, White + Red)")
        self.assertEqual(mtgcolors.clarify("bu"), "Dimir (UB, Blue + Black)")
        self.assertEqual(mtgcolors.clarify("bg"), "Golgari (BG, Black + Green)")
        self.assertEqual(mtgcolors.clarify("gr"), "Gruul (RG, Red + Green)")
        self.assertEqual(mtgcolors.clarify("ru"), "Izzet (UR, Blue + Red)")
        self.assertEqual(mtgcolors.clarify("bw"), "Orzhov (WB, White + Black)")
        self.assertEqual(mtgcolors.clarify("br"), "Rakdos (BR, Black + Red)")
        self.assertEqual(mtgcolors.clarify("gw"), "Selesnya (WG, White + Green)")
        self.assertEqual(mtgcolors.clarify("gu"), "Simic (UG, Green + Blue)")

    def test_3_letters_colors(self):
        expected = "Abzan (WBG, White + Black + Green)"
        self.assertEqual(mtgcolors.clarify("wbg"), expected)
        self.assertEqual(mtgcolors.clarify("wgb"), expected)
        self.assertEqual(mtgcolors.clarify("bwg"), expected)
        self.assertEqual(mtgcolors.clarify("bgw"), expected)
        self.assertEqual(mtgcolors.clarify("gbw"), expected)
        self.assertEqual(mtgcolors.clarify("ruw"), "Jeskai (WUR, White + Blue + Red)")
        self.assertEqual(mtgcolors.clarify("bgu"), "Sultai (UBG, Blue + Black + Green)")
        self.assertEqual(mtgcolors.clarify("brw"), "Mardu (WBR, White + Black + Red)")
        self.assertEqual(mtgcolors.clarify("gru"), "Temur (URG, Blue + Red + Green)")
        self.assertEqual(mtgcolors.clarify("guw"), "Bant (WUG, White + Blue + Green)")
        self.assertEqual(mtgcolors.clarify("buw"), "Esper (WUB, White + Blue + Black)")
        self.assertEqual(mtgcolors.clarify("bru"), "Grixis (UBR, Blue + Black + Red)")
        self.assertEqual(mtgcolors.clarify("bgr"), "Jund (BRG, Black + Red + Green)")
        self.assertEqual(mtgcolors.clarify("grw"), "Naya (WRG, White + Red + Green)")

    def test_4_letters_colors(self):
        self.assertEqual(
            mtgcolors.clarify("bruw"), "Artifice (WUBR, White + Blue + Black + Red)"
        )
        self.assertEqual(
            mtgcolors.clarify("bgur"), "Chaos (UBRG, Blue + Black + Red + Green)"
        )
        self.assertEqual(
            mtgcolors.clarify("bgrw"), "Aggression (BRGW, Black + Red + Green + White)"
        )
        self.assertEqual(
            mtgcolors.clarify("gruw"), "Altruism (RGWU, Red + Green + White + Blue)"
        )
        self.assertEqual(
            mtgcolors.clarify("bguw"), "Growth (GWUB, Green + White + Blue + Black)"
        )

    def test_5_colors(self):
        expected = "Rainbow (WUBRG, White + Blue + Black + Red + Green)"
        self.assertEqual(mtgcolors.clarify("rainbow"), expected)
        self.assertEqual(mtgcolors.clarify("wubrg"), expected)

    def test_verbose(self):
        actual = mtgcolors.clarify("uw", True)
        lines = actual.splitlines()
        self.assertGreater(len(lines), 1)
        self.assertEqual(lines[0], "Azorius (WU, White + Blue)")

    def test_wildcard(self):
        self.assertEqual(
            mtgcolors.clarify("ux"),
            """Available choices are:
* Azorius (WU, White + Blue)
* Dimir (UB, Blue + Black)
* Izzet (UR, Blue + Red)
* Simic (UG, Green + Blue)""",
        )
        self.assertEqual(
            mtgcolors.clarify("uwx"),
            """Available choices are:
* Bant (WUG, White + Blue + Green)
* Esper (WUB, White + Blue + Black)
* Jeskai (WUR, White + Blue + Red)""",
        )
        self.assertEqual(
            mtgcolors.clarify("uwrx"),
            """Available choices are:
* Altruism (RGWU, Red + Green + White + Blue)
* Artifice (WUBR, White + Blue + Black + Red)""",
        )
        self.assertEqual(
            mtgcolors.clarify("uwxx"),
            """Available choices are:
* Altruism (RGWU, Red + Green + White + Blue)
* Artifice (WUBR, White + Blue + Black + Red)
* Growth (GWUB, Green + White + Blue + Black)""",
        )
        self.assertEqual(
            mtgcolors.clarify("uwxxx"),
            """Available choices are:
* Rainbow (WUBRG, White + Blue + Black + Red + Green)""",
        )

    def test_alias(self):
        pairs = {
            "mono-rakdos": "Rakdos (BR, Black + Red)",
            "necra": "Abzan (WBG, White + Black + Green)",
            "junk": "Abzan (WBG, White + Black + Green)",
            "teneb": "Abzan (WBG, White + Black + Green)",
            "doran": "Abzan (WBG, White + Black + Green)",
            "raka": "Jeskai (WUR, White + Blue + Red)",
            "numot": "Jeskai (WUR, White + Blue + Red)",
            "ana": "Sultai (UBG, Blue + Black + Green)",
            "bug": "Sultai (UBG, Blue + Black + Green)",
            "vorosh": "Sultai (UBG, Blue + Black + Green)",
            "dega": "Mardu (WBR, White + Black + Red)",
            "oros": "Mardu (WBR, White + Black + Red)",
            "ceta": "Temur (URG, Blue + Red + Green)",
            "rug": "Temur (URG, Blue + Red + Green)",
            "intet": "Temur (URG, Blue + Red + Green)",
            "yore": "Artifice (WUBR, White + Blue + Black + Red)",
            "yore-tiller": "Artifice (WUBR, White + Blue + Black + Red)",
            "non-green": "Artifice (WUBR, White + Blue + Black + Red)",
            "glint": "Chaos (UBRG, Blue + Black + Red + Green)",
            "glint-eye": "Chaos (UBRG, Blue + Black + Red + Green)",
            "non-white": "Chaos (UBRG, Blue + Black + Red + Green)",
            "dune": "Aggression (BRGW, Black + Red + Green + White)",
            "dune-brood": "Aggression (BRGW, Black + Red + Green + White)",
            "non-blue": "Aggression (BRGW, Black + Red + Green + White)",
            "ink": "Altruism (RGWU, Red + Green + White + Blue)",
            "ink-treader": "Altruism (RGWU, Red + Green + White + Blue)",
            "non-black": "Altruism (RGWU, Red + Green + White + Blue)",
            "witch": "Growth (GWUB, Green + White + Blue + Black)",
            "witch-maw": "Growth (GWUB, Green + White + Blue + Black)",
            "non-red": "Growth (GWUB, Green + White + Blue + Black)",
            "domain": "Rainbow (WUBRG, White + Blue + Black + Red + Green)",
            "five-color": "Rainbow (WUBRG, White + Blue + Black + Red + Green)",
        }
        for argument, expected in pairs.items():
            with self.subTest(i=argument):
                self.assertEqual(mtgcolors.clarify(argument), expected)


if __name__ == "__main__":
    unittest.main()
